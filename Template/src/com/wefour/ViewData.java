

public class ViewData {
	//  get data from customer orders
	private String date; 
	private String coustomername;
	private String enginename;
	private String quantity;
	
	//set data by admin
	private String capacity;
	private String fueltype;
	private String mounttype;
	private String cost;
	
	
	// getter and setter methods
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getEnginename() {
		return enginename;
	}
	public void setEnginename(String enginename) {
		this.enginename = enginename;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	
	// getter method
	
	public String getCoustomername() {
		return coustomername;
	}
	
	// setter methods
	public void setCapasity(String capacity) {
		this.capacity = capacity;
	}
	public String setFueltype(String fueltype) {
		this.fueltype=fueltype;
	}
	public void setMounttype(String mounttype) {
		this.mounttype = mounttype;
	}
	public String setCost(String cost) {
		this.cost=cost;
	}
	public void setMake(String make) {
		this.make = make;
	}
	
	
}
