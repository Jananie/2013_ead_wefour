package com.wefour;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class CustomerServlet extends HttpServlet {

	public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		RequestDispatcher dispatcher;
		boolean flag;
		String Engine_Number = request.getParameter("Engine_Number");
		String Item_Quantity = request.getParameter("Item_Quantity");
		String Expected_Date = request.getParameter("Expected_Date");
		String Shipping_Method = request.getParameter("Shipping_Method");
		
		System.out.println("Engine_Number:"+Engine_Number+ "Item_Quantity:"+Item_Quantity +"Expected_Date:" +Expected_Date+
				"Shipping_Method:"+ Shipping_Method);
		
		flag = CustomerData.createorUpdateCustomerData(Engine_Number,Item_Quantity,Expected_Date,Shipping_Method);
		System.out.println("The flag value is:"+ flag);
		if (flag)
		{
			System.out.println("Success");
			dispatcher = getServletContext().getRequestDispatcher("/category.html/");
			dispatcher.forward(request, response);
		}else
		{
			System.out.println("Failed");
			dispatcher = getServletContext().getRequestDispatcher("/orderplace.html/");
			dispatcher.forward(request, response);
		}
		
		
	}
}
