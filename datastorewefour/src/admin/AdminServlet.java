package admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class AdminServlet extends HttpServlet {

	public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		RequestDispatcher dispatcher;
		boolean flag;
		String Engine_Number = request.getParameter("Engine_Number");
		String Engine_Type = request.getParameter("Engine_Type");
		String No_of_available_engine = request.getParameter("No_of_available_engine");
		String Price = request.getParameter("Price");
		
		flag = AdminData.createorUpdateAdminData(Engine_Number,Engine_Type,No_of_available_engine,Price);
		System.out.println("The flag value is:"+ flag);
		if (flag)
		{
			System.out.println("Success");
			dispatcher = getServletContext().getRequestDispatcher("/admin.html");
			dispatcher.forward(request, response);
		}else
		{
			System.out.println("Failed");
			dispatcher = getServletContext().getRequestDispatcher("adminform.html");
			dispatcher.forward(request, response);
		}
		
		
	}
}
