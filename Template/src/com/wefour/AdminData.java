package com.wefour;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
public class AdminData {

	private static DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public static boolean createorUpdateAdminData(String Engine_Number,String Engine_Type,String No_of_available_engine,String Price)
	{
		System.out.println("in createorupdate engine");
		Entity admin=new Entity("Admin",Engine_Number);
	    admin.setProperty("Engine_Number",Engine_Number);
	    admin.setProperty("Engine_Type",Engine_Type);
	    admin.setProperty("No_of_available_engines",No_of_available_engine);
	    admin.setProperty("Price",Price);
		
		System.out.println("the Datastore"+ datastore.put(admin));
		return true;
		
	}
	public static Iterable<Entity> ListEngines(String kind,String Engine_Number){
		System.out.println("The kind is: " +kind+ "Engine Number is"+Engine_Number);
		Query q =new Query(kind).addSort("Engine_Number",Query.SortDirection.ASCENDING);
		Iterable<Entity> pq = datastore.prepare(q).asIterable(FetchOptions.Builder.withLimit(5));
		return pq;
	}
}
