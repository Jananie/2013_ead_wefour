package com.wefour;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
public class CustomerData {

	private static DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public static boolean createorUpdateCustomerData(String Engine_Number,String Item_Quantity,String Expected_Date,String Shipping_Method)
	{
		System.out.println("new engine order");
		Entity customer=new Entity("customer",Engine_Number);
		customer.setProperty("Engine_Number",Engine_Number);
		customer.setProperty("Item_Quantity",Item_Quantity);
		customer.setProperty("Expected_Date",Expected_Date);
		customer.setProperty("Shipping_Method",Shipping_Method);
		
		System.out.println("the Datastore"+ datastore.put(customer));
		return true;
		
	}
	public static Iterable<Entity> ListEngines(String kind,String Engine_Number){
		System.out.println("The kind is: " +kind+ "Engine Number is"+Engine_Number);
		Query q =new Query(kind).addSort("Engine_Number",Query.SortDirection.ASCENDING);
		Iterable<Entity> pq = datastore.prepare(q).asIterable(FetchOptions.Builder.withLimit(5));
		return pq;
	}
}
