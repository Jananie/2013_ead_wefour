package com.wefour;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.xmpp.JID;
import com.google.appengine.api.xmpp.Message;
import com.google.appengine.api.xmpp.MessageBuilder;
import com.google.appengine.api.xmpp.SendResponse;
import com.google.appengine.api.xmpp.XMPPService;
import com.google.appengine.api.xmpp.XMPPServiceFactory;

public class ViewItemServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		
		DatastoreService dataStore = DatastoreServiceFactory.getDatastoreService();
		Query getUsers = new Query("Item");

		ArrayList<ViewData> listUsers = new ArrayList<ViewData>();
		
		ViewData itmview=new ViewData();
		
		for (Entity webUser : dataStore.prepare(getUsers).asIterable()) {
						
			itmview.setTitle((String)webUser.getProperty("Date"));
			itmview.setCondition((String)webUser.getProperty("Costomer Name"));
			itmview.setMake((String)webUser.getProperty("Engine Name"));
			itmview.setModel((String)webUser.getProperty("Quantity"));
			//itmview.setModelNo((String)webUser.getProperty("ModelNumber"));
			//itmview.setYear((String)webUser.getProperty("Year"));
			//itmview.setInfo((String)webUser.getProperty("AdditionalInfo"));
			//itmview.setPrice((String)webUser.getProperty("Price"));
						
			listUsers.add(itmview);
		}
		resp.setContentType("text/html");
		//for(BFCItem bfc:listUsers){
		//resp.getWriter().println(bfc.getModel()+"</br>"+bfc.getTitle()+"</br>"+bfc.getCondition()+"</br>"+bfc.getMake()+"</br>"+bfc.getModel()+"</br>"+bfc.getModelNo()+"</br>"+bfc.getModelNo()+"</br></br></br>");
		//System.out.println(bfc);
			
		//}
		//resp.getWriter().println("successfuly view irems....");
		try {
	        // Set the attribute and Forward to hello.jsp
	        req.setAttribute ("items", listUsers); // to save your temporary calculations. 
	        req.getRequestDispatcher("/Store.jsp").forward(req, resp);
	    } catch (Exception ex) {
	        ex.printStackTrace ();
	    }
	}
}

		
