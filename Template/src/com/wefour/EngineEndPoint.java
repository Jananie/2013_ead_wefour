package com.wefour;

import java.util.ArrayList;
import java.util.List;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;

@Api(name="engineapi")
public class EngineEndPoint {

	@ApiMethod(path="getEngineList")
	public List getEngines(){
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Query q = new Query("Engine");
		PreparedQuery pq = datastore.prepare(q);
		
		List list = new ArrayList<>();
		
		for(Entity e : pq.asIterable()){
			list.add(e);
		}
		
		return list;
		
	}
	
}
